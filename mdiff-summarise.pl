#!/usr/bin/perl -n
next if m/^\+/;
if (m/^[- ](\d+)$/) {
    $nlno= $1;
    next;
} elsif (defined $nlno) {
    m/^[- ]     *\S/ or die;
    s/^([- ])    / $1. sprintf "%-6d ", $nlno /e;
    undef $nlno;
}
die if defined $lno;
next if m/^[- ]    \s\s/;
next if m/^\-\-\-|^\@\@/;
next if m/^ /;
die unless m/^\-/;
print or die $!;
