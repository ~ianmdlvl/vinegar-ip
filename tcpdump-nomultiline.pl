#!/usr/bin/perl -p
if (m/^\t\t\t 4[0-9a-f]{3}(?: [0-9a-f]{4}){7}/) {
    s/^/\n/ if $glerk;
    $glerk=0;
} elsif ($glerk) {
    s,^, // ,; s/\n$//;
} elsif (m/^ ?[^ \t]/) {
    s/\n$//; $glerk=1;
}
# $Id$
