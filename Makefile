# Makefile for vinegar-ip
#
# This file is part of vinegar-ip, tools for IP transparency testing.
# vinegar-ip is Copyright (C) 2002 Ian Jackson
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. 
#
# $Id$

#############################################################
# You should edit the parameters below for your site

SOURCE=		130.233.5.88/00:0D:60:89:9F:AC
DEST=		193.201.200.170/00:0B:45:B3:78:40

UNIQUE=
# set UNIQUE to something random for less observability

# NB, this MTU is not completely strictly adhered to by the
# test packet generator.  Sorry.
MTU=		100

# no of packets in each individual part, including part 1
PERPART=	100

# REST is made of PARTS-1 parts of PERPART packets
PARTS=		100

# You shouldn't need to edit anything beyond this point.
#############################################################

SCRIPT_TARGETS=	on-dest.sh monitor.sh

FEW_TARGETS=	$(SCRIPT_TARGETS) \
		send-1.pcap send-1.log send-1.why

TARGETS=	$(FEW_TARGETS) \
		send-all.pcap send-all.log send-all.why

A_PARTNOS=	$(shell \
	set -e; i=1; while [ $$i -le $(PARTS) ]; do \
		echo $$i; i=$$(( $$i + 1)); done \
	)

A_BASES=	$(addprefix send-,$(A_PARTNOS))
A_PCAPS=	$(addsuffix .pcap,$(A_BASES))
A_WHYS=		$(addsuffix .why,$(A_BASES))

AN_BASES=	$(basename $(wildcard recv-*.pcap))
AN_LOGS=	$(addsuffix .log,$(AN_BASES))
AN_DIFFS=	$(addsuffix .diff,$(AN_BASES))
AN_MDIFFS=	$(addsuffix .mdiff,$(AN_BASES))
AN_SUMMARIES=	$(addsuffix .summary,$(AN_BASES))
AN_TARGETS=	$(AN_LOGS) $(AN_DIFFS) $(AN_MDIFFS) $(AN_SUMMARIES)

INFORM=		@echo '	GENERATED THESE FILES:'; \
		echo '		$^'

SOURCE_IP=	$(shell expr $(SOURCE) : '\([0-9.]*\)/')
DEST_IP=	$(shell expr $(DEST) : '\([0-9.]*\)/')

all:		$(TARGETS)
			$(INFORM)

few:		$(FEW_TARGETS)
			$(INFORM)

scripts:	$(SCRIPT_TARGETS)
			$(INFORM)

anal analyse:	$(AN_TARGETS)
			$(INFORM)

send-all.pcap:	$(A_PCAPS)
		rm -f $@
		dd if=$< ibs=24 count=1 of=$@
		set -e; for f in $(A_PCAPS); do \
			dd ibs=24 skip=1 if=$$f >>$@; done

send-all.why:	$(A_WHYS) Makefile
		cat $(A_WHYS) >$@.1.tmp
		nl -bp'^ ? ? ?[0-9]' <$@.1.tmp >$@.2.tmp
		@mv -f $@.2.tmp $@

send-%.pcap send-%.why:	./make-probes.tcl
	./make-probes.tcl --write $@ --mtu $(MTU) --upto $(PERPART) \
	        --source $(SOURCE) \
	        --dest $(DEST) \
	         --xseed "$* $(UNIQUE)" >send-$*.why

%.log:	%.pcap Makefile \
	lnumber-tcpdump.pl blank-ttl-ipcsum.pl tcpdump-nomultiline.pl
		tcpdump -tnxvvs$$(($(MTU)+500)) -r $< >$@.2.tmp
		./tcpdump-nomultiline.pl <$@.2.tmp >$@.3.tmp
		./blank-ttl-ipcsum.pl <$@.3.tmp >$@.4.tmp
		./lnumber-tcpdump.pl <$@.4.tmp >$@.5.tmp
		@mv -f $@.5.tmp $@

recv-%.diff:	send-%.log recv-%.log
		diff -uI'^[0-9]' $^ >$@ || test $$? == 1

recv-%.mdiff:	send-%.log recv-%.log
		diff -U 1 -I'^[0-9]\|^    [ 	][ 	]' $^ >$@ \
			|| test $$? == 1

recv-%.summary:	recv-%.mdiff mdiff-summarise.pl
		./mdiff-summarise.pl <$< >$@.1.tmp
		sort -t : +1 <$@.1.tmp >$@.2.tmp
		@mv -f $@.2.tmp $@

%.sh:		%.template Makefile
		sed <$< >$@.tmp -e ' \
			s/@@M/'$$(($(MTU)+500))'/; \
			s/@@S/$(SOURCE_IP)/; \
			s/@@D/$(DEST_IP)/'
		chmod +x $@.tmp
		@mv -f $@.tmp $@

clean:
		rm -f *.tmp *~ t u v

realclean:	clean
		rm -f $(TARGETS) *.pcap *.why *.log recv-*.diff

# $Id$
